using System;
using System.Globalization;
using System.Text;

#pragma warning disable CA1822

namespace TransformToWordsTask
{
    public sealed class Transformer
    {
        public string TransformToWords(double number)
        {
            string tempWord = number.ToString("G", CultureInfo.CurrentCulture);
            string numWord = string.Empty;

            StringBuilder builder = new StringBuilder();

            switch (number)
            {
                case double.PositiveInfinity:
                    return "Positive Infinity";
                case double.NegativeInfinity:
                    return "Negative Infinity";
                case double.Epsilon:
                    return "Double Epsilon";
                case double.NaN:
                    return "NaN";
                default:
                    if (double.IsNegative(number))
                    {
                        builder.Append("minus ");
                    }

                    for (int i = 0; i < tempWord.Length; i++)
                    {
                        switch (tempWord[i])
                        {
                            case '0':
                                builder.Append("zero ");
                                break;
                            case '1':
                                builder.Append("one ");
                                break;
                            case '2':
                                builder.Append("two ");
                                break;
                            case '3':
                                builder.Append("three ");
                                break;
                            case '4':
                                builder.Append("four ");
                                break;
                            case '5':
                                builder.Append("five ");
                                break;
                            case '6':
                                builder.Append("six ");
                                break;
                            case '7':
                                builder.Append("seven ");
                                break;
                            case '8':
                                builder.Append("eight ");
                                break;
                            case '9':
                                builder.Append("nine ");
                                break;
                            case '.':
                                builder.Append("point ");
                                break;
                            case 'E':
                                builder.Append("E plus ");
                                break;
                        }
                    }

                    numWord = builder.ToString();

                    numWord = numWord.Remove(numWord.Length - 1);
                    char[] res = numWord.ToCharArray();
                    res[0] = char.ToUpper(res[0], CultureInfo.CurrentCulture);
                    return new string(res);
            }
        }
    }
}